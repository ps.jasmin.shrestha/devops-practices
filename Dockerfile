FROM maven:3.6.3-jdk-8
LABEL maintainer="shresthajazz741@gmail.com"
ARG IMAGETAG
ENV ENTRY_ARG=$IMAGETAG
RUN useradd -ms /bin/bash springapp
RUN chown -R springapp:springapp  "/opt"
COPY [ "target/assignment-$IMAGETAG.jar","entrypoint.sh","/opt/" ]
WORKDIR "/opt/"
ENTRYPOINT ["/opt/entrypoint.sh"]
